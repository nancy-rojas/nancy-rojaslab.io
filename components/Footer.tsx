import React from "react";

const Footer = () => {
  return (
    <footer className="flex flex-row items-start justify-center w-full py-8 bg-black text-[12px] font-light border-t border-neutral-800 ">
      <div className="w-full">
        <div className="flex flex-row items-start  text-center justify-center px-10 xl:px-20">
          <div className="flex text-center footer-contact text-md-start ">
            <p className="text-neutral-500">Nancy Rojas © 2024</p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
