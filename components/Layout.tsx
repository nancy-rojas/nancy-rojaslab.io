import Head from "next/head";
import React from "react";
import Footer from "./Footer";

export type LayoutProps = {
   children: React.ReactNode;
   title?: string;
};

const Layout = ({ children, title }: LayoutProps) => {
   return (
      <div className="flex flex-col items-center">
         <Head>
            <title>{title ?? "Nancy Rojas"}</title>
         </Head>
         <div className="w-full">{children}</div>
         <Footer />
      </div>
   );
};

export default Layout;
