import { Icon } from "@iconify/react";
import Link from "next/link";
import React from "react";

// Navbar only for Project View

const Navbar = () => {
  return (
    <>
      <header id="header" className=" z-[100] w-full px-8 py-6">
        <div className="flex items-center justify-between">
          <Link href={"/"}>
            <div className="transition-all flex flex-row items-center gap-1 text-white hover:text-neutral-700">
              <Icon
                icon="material-symbols:chevron-left"
                className=" text-4xl"
              />{" "}
              <span className="text-2xl">Back</span>
            </div>
          </Link>
        </div>
      </header>
    </>
  );
};

export default Navbar;
