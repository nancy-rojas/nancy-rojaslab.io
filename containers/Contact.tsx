import { Icon } from "@iconify/react";
import Link from "next/link";
import React from "react";

const Contact = () => {
  return (
    <section
      id="contact"
      className="contact bg-black flex items-center justify-center h-[50vh] md:h-[50vh] lg:h-[50vh] xl:h-[50vh] 2xl:h-[80vh]"
    >
      <div
        className="sm:w-50 2xl:w-50 xl:w-50 lg:w-50 md:w-50"
        data-aos="fade-up"
        data-aos-duration="960"
      >
        <Link
          href="https://www.upwork.com/freelancers/~0119ae3ee2b15daee0"
          target="_blank"
          className="flex flex-col items-center  "
        >
          <header>
            <p className="font-medium transition-all text-white hover:text-neutral-700 text-2xl leading-tight 2xl:text-6xl 2xl:leading-tight xl:text-5xl xl:leading-tight lg:text-5xl lg:leading-tight md:text-5xl md:leading-tight text-center  ">
              Let&apos;s talk about your project{" "}
              <Icon icon="ic:baseline-arrow-outward" className="inline" />
            </p>
          </header>
        </Link>
      </div>
    </section>
  );
};

export default Contact;
