import Image from "next/image";
import Link from "next/link";
import React from "react";
import { PortfolioModel } from "../lib/models/portfolio.model";
import { motion, AnimatePresence } from "framer-motion";
import Tag from "../components/Tag";
import { Icon } from "@iconify/react";

export type PortfolioProps = {
   projects: PortfolioModel[];
};

const Portfolio = ({ projects }: PortfolioProps) => {
   return (
      <section id="portfolio" className="">
         <div
            className="w-full px-8 xs:px-20 sm:px-20 md:px-40 lg:px-40 xl:px-40"
            data-aos="fade-up"
         >
            <div
               className="grid gap-8 transition-all xs:grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-2 2xl:grid-cols-3"
               data-aos="fade-up"
               data-aos-delay="100"
            >
               <AnimatePresence presenceAffectsLayout>
                  {projects &&
                     projects.map((p) => (
                        <motion.div
                           key={p.id}
                           initial={{ opacity: 0 }}
                           animate={{ opacity: 1 }}
                           exit={{ opacity: 0 }}
                           className={`filter-${p.category}`}
                           layout
                        >
                           <Link href={`/projects/${p.id}`}>
                              <div className="relative transition-all cursor-pointer rounded-3xl group">
                                 <Image
                                    src={p.imgUrl}
                                    className="transition-all rounded-3xl mx-auto"
                                    alt=""
                                    width={p.width}
                                    height={p.height}
                                 />
                                 <div className=" absolute top-0 bottom-0 left-0 right-0 z-50 flex flex-col items-center justify-center p-4 text-center transition-all opacity-0 rounded-3xl backdrop-blur-md bg-black/60 group-hover:opacity-100">
                                    <h4 className="text-xl flex flex-row items-center font-semibold text-white gap-2">
                                       {p.title}

                                       <Icon
                                          icon="ic:baseline-arrow-outward"
                                          className="text-white text-2xl"
                                       />
                                    </h4>
                                    <br />
                                    <div className="flex-col gap-1 mb-2 text-neutral-300 flex">
                                       {p.tags &&
                                          p.tags.map((t) => (
                                             <Tag
                                                key={t.id}
                                                tag={t.name}
                                                icon={t.icon}
                                             />
                                          ))}
                                    </div>
                                 </div>
                              </div>
                           </Link>
                        </motion.div>
                     ))}
               </AnimatePresence>
            </div>
         </div>
      </section>
   );
};

export default Portfolio;
