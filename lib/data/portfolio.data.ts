import { PortfolioModel } from '../models/portfolio.model';

export const portfolioData: PortfolioModel[] = [
		{
		id: 'weather-widget',
		title: 'Weather Widget',
		category: 'mobile',
		tags: [
			{
				id: 'weatherwidget1',
				class: 'info',
				name: 'HTML',
				icon: 'icomoon-free:html-five',
			},
			{
				id: 'weatherwidget2',
				class: 'info',
				name: 'CSS',
				icon: 'uiw:css3',
			},
		],
		imgUrl: '/weather-widget/thumb.jpg',
		repoUrl: 'https://github.com/nancy-rojas/weather-widget',
		width: 1200,
		height: 1000,
	},
	{
		id: 'tipsy-design',
		title: 'Tipsy Web Application',
		category: 'mobile',
		tags: [
			{
				id: 'tipsydesign',
				class: 'info',
				name: 'Figma',
				icon: 'ph:figma-logo',
			},
		],
		imgUrl: '/tipsy-design/thumb.jpg',
		width: 1200,
		height: 1000,
	},
	{
		id: 'jayu-design',
		title: 'Jayu Landing Page',
		category: 'web',
		tags: [
			{
				id: 'jayudesign',
				class: 'info',
				name: 'Figma',
				icon: 'ph:figma-logo',
			},
					],
		imgUrl: '/jayu-design/thumb.jpg',
		figmaUrl: 'https://www.figma.com/file/2sX5sdIZPEFGMwek92lPcF/%F0%9F%9F%A3-Jayu_Web-redesign_shared-portfolio?node-id=1113%3A2128&t=LgU0ofwxInxBQUJt-1',
		width: 1200,
		height: 1000,
	},
	{
		id: 'forum-design',
		title: 'Forum Web Application',
		category: 'web',
		tags: [
			{
				id: 'forumdesign',
				class: 'info',
				name: 'Figma',
				icon: 'ph:figma-logo',
			},
		],
		imgUrl: '/forum-design/thumb.jpg',
		width: 1200,
		height: 1000,
	},
	{
		id: 'element-design',
		title: 'Element Mobile App',
		category: 'mobile',
		tags: [
			{
				id: 'elementdesign',
				class: 'info',
				name: 'Figma',
				icon: 'ph:figma-logo',
			},
		],
		imgUrl: '/element-design/thumb.jpg',
		width: 1200,
		height: 1000,
	},
	{
		id: 'timevity-design',
		title: 'Timevity Mobile App',
		category: 'mobile',
		tags: [
			{
				id: 'timevitydesign',
				class: 'info',
				name: 'Figma',
				icon: 'ph:figma-logo',
			},
		],
		imgUrl: '/timevity-design/thumb.jpg',
		linkUrl: 'https://apps.apple.com/us/app/timevity/id1610670575',
		width: 1200,
		height: 1000,
	},
];
