import { Icon } from "@iconify/react";
import type { NextPage, NextPageContext } from "next";
import Link from "next/link";
import Layout from "../components/Layout";
import Contact from "../containers/Contact";
import Portfolio from "../containers/Portfolio";
import { portfolioData } from "../lib/data/portfolio.data";
import { PortfolioModel } from "../lib/models/portfolio.model";

export type HomePageProps = {
  projects: PortfolioModel[];
};

const HomePage: NextPage<HomePageProps> = ({ projects }: HomePageProps) => {
  return (
    <Layout title="Nancy Rojas">
      <section
        className="flex-col items-center w-full justify-center mt-10"
        data-aos="fade-up"
        data-aos-duration="960"
      >
        <div className="flex flex-col items-center justify-center ">
          <div className="flex flex-col items-center gap-8 px-8 sm:px-20 md:px-40 lg:px-40 xl:px-40 font-normal text-left col-12">
            <h1 className="text-2xl 2xl:text-2xl 2xl:leading-tight xl:text-2xl lg:text-2xl lg:leading-tight md:text-2xl md:leading-tight">
              Hello! ✦ <br />
              <br />
              I&apos;m Nancy, a Web Designer and aspiring Web Developer with
              hands-on experience in Figma. I have also worked with small
              projects using HTML5, CSS, Tailwind, and React. I&apos;m very
              passionate about designing responsive and user-friendly interfaces
              for websites and believe in continuous learning and improvement.
              For now, I&apos;m only taking design projects, but I will keep you
              posted about new additions to my services!
              <br />
              <br />
              The following are selected projects all human-made by me. Feel
              free to contact me for any design inquiries and bring your ideas
              to life 🚀.
            </h1>
            <br />
            <div className="w-full flex flex-col items-start text-2xl 2xl:text-6xl xl:text-5xl lg:text-5xl md:text-5xl"></div>
          </div>
        </div>
      </section>

      <main id="main">
        <Portfolio projects={projects} />

        <Contact />
      </main>
    </Layout>
  );
};

export async function getStaticProps(context: NextPageContext) {
  const projects = portfolioData;

  return {
    props: {
      projects,
    },
  };
}

export default HomePage;
