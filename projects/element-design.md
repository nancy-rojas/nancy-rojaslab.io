![Element Mobile App](/element-design/1.jpg)

The presentation displays a login page with sign up option. Dashboard page contains an offer of one week of a Plus membership for free, survey data, survey templates options and recent files.

Navbar links to dashboard, surveys, statistics and profile settings. Surveys screen display an offer banner, sorting tabs and a list with projects and user's cases.

![Color Details](/element-design/2.jpg)

The combination of emerald and gray colors promote a sense of calm and clarity, which give trust to users in the moment of filling a survey.
