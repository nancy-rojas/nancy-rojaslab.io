![Forum Web Concept](/forum-design/1.jpg)

This project allows sharing posts between users of this application, in which they can share thoughts, reply with information and vote for the most insightful questions. All in a secure way to ensure data consistency and privacy between users.

The main page displays a menu to navigate between sections of the web app. A list of relevant questions for logged users. Number of followers, bandages, trend topics and recent activities from connections.

![Main Page Details](/forum-design/2.jpg)

The dark interface is modern with a pop of violet color intended for young audiencies.
