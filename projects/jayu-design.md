![Jayu Landing Page](/jayu-design/1.jpg)

Jayu is a customer loyalty platform that provides retention tools for small business such as automated campaigns, scheduled messages, customer data and a loyalty mobile app.

My clients needed a redesign of their landing page with the provided branding and shared document with the text content.

![Jayu Landing Page](/jayu-design/2.jpg)

The main page displays the services details and the tools for customer retention. I designed custom icons with their branding color scheme.

All the sections through the landing page have their own CTA buttons. There is also a free guide download section to lead business owners to concrete the acquisition of a subscription by collecting their email.

At the end of the website added a filling form that collects business owner's data and displays contact information.

![Jayu Main Screen Tablet Software](/jayu-design/3.jpg)
