
![Timevity Mobile App - Tablet](/timevity-design/1.jpg)

Timevity is a personal time tracker that promotes good performance and productivity by organizing the time spend in user's daily activities.

The main screen displays a navigation menu that links to dashboard, goals, activities and projects. Information related to the activities and time spend. Charts that display visual data of time with filter options: week, month, year and all time. A list of recent activities, each with a play button to start tracking time in just one tap.

![Timevity Mobile App](/timevity-design/2.jpg)

Mobile version of the app displays an overview of tracked time, create new menu to chose between to create a new activity or project. Create activity screen contains how user fill with the information of an activity with the option to create reminders.

![Timevity Colors Details](/timevity-design/3.jpg)

The dark interface helps to focus on the displayed data and helps to rest from the light of devices. Bright blues inspire loyalty, confidence and security.