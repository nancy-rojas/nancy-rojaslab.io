![Tipsy Web Application](/tipsy-design/1.jpg)

Tipsy is a web application that makes it easier for restaurants and coffee shops to receive tips from their costumers through electronic transactions.

Once the user scans the QR code with their phone, it links to a main screen with the business information. User can select the amount that wants to tip before payment processing.

Payment processing screen is meant to have no distractions, having functionality as priority.

After the payment there is a celebration screen with accent colors, confetti and an option that links customers to write a Google review for the business.

![Tipsy Screen Details](/tipsy-design/2.jpg)
